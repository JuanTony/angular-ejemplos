import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'https://identitytoolkit.googleapis.com/v1';
  private apiKey = 'AIzaSyCODbLRav5VTYmprHifjb47zuBkWztMMpM';
  private userToken : string;
  //crear usuarios
  //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  //log In
  //https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

  constructor( private http: HttpClient) { 
    this.leerToken();
  }

  logout()  {
    localStorage.removeItem('token');
  }

  login(usuario: UsuarioModel) {
    const authData  = {
      //email: usuario.email,
      //password: usuario.password,
      ...usuario,
      returnSecureToken : true  
    };

    return this.http.post(`${ this.url }/accounts:signInWithPassword?key=${ this.apiKey }`, 
              authData).pipe( map( response => {
                this.guardarToken(response['idToken']); 
                return response;
             }));
  }

  nuevoUsuario(usuario: UsuarioModel) : Observable<Object>{
    const authData  = {
      //email: usuario.email,
      //password: usuario.password,
      ...usuario,
      returnSecureToken : true  
    };

    return this.http.post(`${ this.url }/accounts:signUp?key=${ this.apiKey }`, 
            authData).pipe( map( response => {
               this.guardarToken(response['idToken']); 
               return response;
            }));
  }


  private guardarToken (idToken : string) {
    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setDate( 3600 );
    localStorage.setItem('expira', hoy.getTime().toString());
  }

  leerToken () {
    if (localStorage.getItem('token')) 
      this.userToken = localStorage.getItem('token')
    else
      this.userToken = '';
    
    return this.userToken;
  }

  estaAutenticado() : boolean {
    return this.userToken.length > 2;
  }
}
