import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login : UsuarioModel;
  recordarme : boolean;
  constructor( private auth: AuthService,
              private router : Router) { }

  ngOnInit() {
    this.login = new UsuarioModel();
    if (localStorage.getItem('email')) {
      this.login.email =  localStorage.getItem('email');
      this.recordarme = true;
    }
  }

  onSubmit( form : NgForm) {
    if (form.invalid) {
      return;
    }
    
    Swal.fire({
      allowOutsideClick: false,
      type : 'info',
      text: 'Espere por favor...',
    });
    Swal.showLoading();

    this.auth.login(this.login).subscribe(
      (response) => { 
        console.log(response); 
        Swal.close();
        if (this.recordarme) {
          localStorage.setItem('email', this.login.email);
        }
        this.router.navigateByUrl("/home");
      },
      (err) => { 
        console.log(err.error.error.message);
        Swal.fire({
          title: 'Error',
          text: err.error.error.message,
          type : 'error',
        });
      }
    );
  }

}
