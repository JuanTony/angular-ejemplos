import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PaisService } from 'src/app/services/pais.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  usuario = {
    nombre : 'Ramiro',
    apellido:'Rangel',
    correo: 'a@a.com',
    pais : 'CRI',
    genero : 'M'
  };

  paises : any[] = [];

  constructor( private paisService : PaisService) { }

  ngOnInit(): void {
    this.paisService.getPaises().subscribe(response => {
      this.paises = response;
      this.paises.unshift({
        nombre : '[Seleccione Pais]',
        codigo : ''
      });
    });
  }

  guardar(forma : NgForm) {
    if (forma.invalid) {
      Object.values( forma.controls ).forEach(c => {
        c.markAllAsTouched();
      })
      return;
    }
    console.log("Guardar...");
    console.log(forma.value);
  }
}
