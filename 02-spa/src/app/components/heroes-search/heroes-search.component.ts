import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService, Heroe } from 'src/app/services/heroes.service';


@Component({
  selector: 'app-heroes-search',
  templateUrl: './heroes-search.component.html',
  styles: [
  ]
})
export class HeroesSearchComponent implements OnInit {

  heroes: Heroe[];

  constructor( private activatedRoute: ActivatedRoute,
               private _heroeService: HeroesService,
               private router : Router
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(p => {
      this.heroes =  this._heroeService.buscarHeroes(p['termino']);
      console.log(this.heroes);
    });
  }

  verHeroe( idx : number) {
    this.router.navigate(['/heroe', idx]);
  }

}
