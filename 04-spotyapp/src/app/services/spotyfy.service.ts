import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SpotyfyService {

  constructor( private http: HttpClient) { }

  getQuery(query : string) {
    const endPoint = `https://api.spotify.com/v1/${ query }`
    const headers = new HttpHeaders({
      'Authorization' : 'Bearer BQCn8OddIK6muGqLBLOpzZKwjtC9k_-5ISBTOniqAFDH6IE4_PtAyzqHiojtUnnn4v3z8HOHDFZz0NU5KqQ'
    });
    return this.http.get(endPoint, { headers});
  }

  getNewReleases() {
    return this.getQuery('browse/new-releases?limit=20')
                      .pipe( map( response => {
                        return response['albums'].items;
                      }) );
  }


  getArtistas(termino : string) {
    return this.getQuery(`search?q= ${ termino } &type=artist&limit=15`)
                      .pipe( map( response => {
                        return response['artists'].items
                      }) );
  }

  getArtista(id : string) {
    return this.getQuery(`artists/${ id }`);
  }

  getTopTracks( id : string ) {
    return this.getQuery(`artists/${ id }/top-tracks?country=US`)
                      .pipe( map( response => {
                        return response['tracks']
                      }) );
  }
}
