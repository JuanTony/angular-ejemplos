import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotyfyService } from '../../services/spotyfy.service';


@Component({
  selector: 'app-artisa',
  templateUrl: './artisa.component.html',
  styles: [
  ]
})
export class ArtisaComponent {

  artista : any = {};
  topTracks : any[] = [];
  loading : boolean;

  constructor( private router : ActivatedRoute, 
               private spotyfyService : SpotyfyService) {

      this.router.params.subscribe(p => {
        this.getArtista(p['id']);
        this.getTopTracks(p['id']);
      });

  }

  getArtista(id : string ) {
    this.loading = true;
    this.spotyfyService.getArtista(id).subscribe(a => {
      console.log(a);
      this.artista = a;
      this.loading = false;
    });
  }

  getTopTracks(id : string ) {
    this.loading = true;
    this.spotyfyService.getTopTracks(id).subscribe(tt => {
      console.log(tt);
      this.topTracks = tt;
      this.loading = false;
    });
  }


}
