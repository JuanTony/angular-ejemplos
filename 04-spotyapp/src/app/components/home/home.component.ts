import { Component, OnInit } from '@angular/core';
import { SpotyfyService } from 'src/app/services/spotyfy.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent {

  nuevasCanciones : any = [];
  loading : boolean;
  error : boolean;
  msgError : string;

  constructor( private spotyfyService: SpotyfyService ) {
    this.loading = true;
    this.spotyfyService.getNewReleases()
    .subscribe( (response : any) => {
      this.nuevasCanciones = response;
      this.loading = false;
    }, ( e ) => {
      this.error = true;
      this.loading = false;
      this.msgError = e.error.error.message;
      console.log(e.error.error.message);
    } );
  }


}
