import { Component, OnInit } from '@angular/core';
import { SpotyfyService } from 'src/app/services/spotyfy.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent {
  artistas : any[] = [];
  loading : boolean = false;
  constructor( private spotify : SpotyfyService) { }

  buscar(termino : string ) {
    this.loading = true;
    this.spotify.getArtistas(termino)
      .subscribe( (response : any) => {
        this.artistas = response;
        this.loading = false;
      });
  }

}
