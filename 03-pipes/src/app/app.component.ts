import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre : string = "Capitan America";
  nombre2 : string = 'jUan anTonIo ranGEL tIRaDo';
  arreglo = [1,2,3,4,5,6,7,8,9,10];
  PI : number = Math.PI;
  pct = 0.235;
  salario : number = 1234.5;
  fecha : Date = new Date();
  idioma : string = 'es';
  videoURL = 'https://www.youtube.com/embed/pEUrhaq2Mrk';
  verpass : boolean = true;

  heroe = {
    nombre : 'Logan',
    clave : 'Wolverine',
    edad : 500,
    direccion : {
      calle : 'First Av',
      casa : 20
    }
  };


  valorPromesa = new Promise<string>((resolve) => {
    setTimeout(() => {
      resolve('Llegaron los datos!!!!');
    }, 4500);
  });

  cambiarIdioma (lang:string)  {
    this.idioma = lang;
  }
  
}
