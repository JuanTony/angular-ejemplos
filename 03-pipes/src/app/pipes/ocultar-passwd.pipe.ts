import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ocultarPasswd'
})
export class OcultarPasswdPipe implements PipeTransform {
  
  transform(value: string, verpass: boolean = false): string {
   /*let hideVal : string = ''; 
    if (verpass) {
      return value;
    } else {
      for (let index = 0; index < value.length; index++) {
        hideVal += '*';
      }
      return hideVal;
    }

    */
   return (verpass) ? value : '*'.repeat(value.length);
  }
}


