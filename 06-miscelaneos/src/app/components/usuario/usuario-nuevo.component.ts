import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-nuevo',
  template: `
    <p>
      usuario-nuevo works!
    </p>
  `,
  styles: [
  ]
})
export class UsuarioNuevoComponent implements OnInit {

  constructor(private router : ActivatedRoute) {
    console.log("PARAMS RUTA HIJA");
    router.parent.params.subscribe(p => {
      console.log(p);
    });
   }

  ngOnInit(): void {
  }

}
