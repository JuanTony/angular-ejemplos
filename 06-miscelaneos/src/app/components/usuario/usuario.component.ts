import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: []
})
export class UsuarioComponent implements OnInit {

  constructor( private router : ActivatedRoute) {
    console.log("PARAMS RUTA padre");
      router.params.subscribe(p => {
        console.log(p);
      })

   }

  ngOnInit(): void {
  }

}
