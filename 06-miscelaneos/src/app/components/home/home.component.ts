import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    
    <app-ng-style></app-ng-style> 
    <app-css></app-css>
    <app-clases></app-clases>
    <app-ng-switch></app-ng-switch>

  <!-- <p appResaltado>Hola mundo estoy en component, ejemplo directiva </p>  -->
  <p [appResaltado]= "'orange'">Hola mundo estoy en component, ejemplo directiva </p> 

  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
